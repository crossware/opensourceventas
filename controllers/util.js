'use strict';
var q         = require('q');

exports.resolve = function (result) {
  return function (result) {
    var deferred = q.defer();
    deferred.resolve(result);
    return deferred.promise;
  };
};

exports.reject = function (result) {
  return function (result) {
    var deferred = q.defer();
    deferred.reject(result);
    return deferred.promise;
  };
};

exports.success = function (res) {
  return function (f) {
    res.send(200, f);
  };
};

exports.error = function (res) {
  return function (f) {
    res.send(510, f);
  };
};


