'use strict';

/**
 * @ngdoc function
 * @name IDIapp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of IDIapp
 */
var q = require('q'),
	Crud = require('./crud');

function MainClass(db, table) {
	//this.db = db;
	//this.crud = new Crud(db, table);
}

// Insert ------------------------------------------------------------
MainClass.prototype.insert = function (obj) {
	console.log('\n MainClass -> Insert');
	var deferred = q.defer();
	this.crud.insert(obj).then(deferred.resolve, deferred.reject);
	return deferred.promise;
};

// Update ------------------------------------------------------------
MainClass.prototype.update = function (id, obj) {
	var deferred = q.defer(),
		query = {_id: id};
	this.crud.update(query, obj).then(deferred.resolve, deferred.reject);
	return deferred.promise;
};

// Delete ------------------------------------------------------------
MainClass.prototype.delete = function (id) {
	var deferred = q.defer(),
		query = {_id: id};
	this.crud.delete(query).then(deferred.resolve, deferred.reject);
	return deferred.promise;
};

// Select ------------------------------------------------------------
MainClass.prototype.select = function (query) {
	var deferred = q.defer();
	this.crud.select(query).then(deferred.resolve, deferred.reject);
	return deferred.promise;
};


module.exports = MainClass;