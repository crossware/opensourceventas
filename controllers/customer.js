'use strict';

/**
 * @ngdoc function
 * @name IDIapp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of IDIapp
 */
var q = require('q'),
	Crud = require('./crud'),
	MainClass = require('./mainClass');

function Customer(db) {
	MainClass.apply(this, Array.prototype.slice.call(arguments));
	this.db = db;
	this.crud = new Crud(db, 'CUSTOMERS');
}

Customer.prototype = new MainClass();

module.exports = Customer;