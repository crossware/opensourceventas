'use strict';

/**
 * @ngdoc function
 * @name IDIapp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of IDIapp
 */
var q = require('q'),
	Crud = require('./crud'),
	MainClass = require('./mainClass');

function User(db) {
	MainClass.apply(this, Array.prototype.slice.call(arguments));
	this.db = db;
	this.crud = new Crud(db, 'USERS');
}

User.prototype = new MainClass();

module.exports = User;