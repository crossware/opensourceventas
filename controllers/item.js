'use strict';

/**
 * @ngdoc function
 * @name IDIapp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of IDIapp
 */
var q = require('q'),
	Crud = require('./crud'),
	MainClass = require('./mainClass');

function Item(db) {
	MainClass.apply(this, Array.prototype.slice.call(arguments));
	this.db = db;
	this.crud = new Crud(db, 'ITEMS');
}

Item.prototype = new MainClass();

module.exports = Item;