'use strict';

/**
 * @ngdoc function
 * @name IDIapp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of IDIapp
 */
var q = require('q'),
	Crud = require('./crud'),
	MainClass = require('./mainClass');

function SupportRequest(db) {
	MainClass.apply(this, Array.prototype.slice.call(arguments));
	this.db = db;
	this.crud = new Crud(db, 'SUPPORTREQUESTS');
}

SupportRequest.prototype = new MainClass();

SupportRequest.prototype.generateReport = function (option) {
	var deferred = q.defer(),
		group = {
			'_id': '',
			count: { $sum: 1 }
		};
	group._id = '$' + option + '.name';
	this.db.get('SUPPORTREQUESTS').col.aggregate([{
		$group: group
	}], {}, function (err, res) {
		if (err) {
			deferred.reject({result: 'Not ok', data: err});
		} else {
			var response = {result: 'Ok', data: res};
			deferred.resolve(response);
		}
	});
	return deferred.promise;
};

module.exports = SupportRequest;