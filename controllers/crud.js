'use strict';

/**
 * @ngdoc function
 * @name IDIapp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of IDIapp
 */
var q = require('q');

function Crud(db, table) {
	this.db = db;
	this.table = table;
}

// Insert ----------------------------------------------------------------------
Crud.prototype.insert = function (obj) {
	var deferred = q.defer();
	this.db.get(this.table).insert(obj, function (err, data) {
		if (err) {
			//deferred.reject({result: 'Not ok', data: err});
			throw err;
		}
		var response = {result: 'Ok', data: data};
		deferred.resolve(response);
	});
	return deferred.promise;
};

// Update ----------------------------------------------------------------------
Crud.prototype.update = function (query, obj) {
	var deferred = q.defer();
	this.db.get(this.table).update(query, {$set: obj}, {multi: true}, function (err, data) {
		if (err) {
			//deferred.reject({result: 'Not ok', data: err});
			throw err;
		}
		var response = {result: 'Ok', data: data};
		deferred.resolve(response);
	});
	return deferred.promise;
};

// Delete -----------------------------------------------------------------------
Crud.prototype.delete = function (query) {
	var deferred = q.defer();
	this.db.get(this.table).remove(query, {justOne: 1}, function (err, data) {
		if (err) {
			throw err;
			//deferred.reject({result: 'Not ok', data: err});
		}
		var response = {result: 'Ok', data: data};
		deferred.resolve(response);
	});
	return deferred.promise;
};


// Select ------------------------------------------------------------------------
Crud.prototype.select = function (query) {
	var deferred = q.defer();
	console.log(query);
	this.db.get(this.table).find(query, function (err, data) {
		if (err) {
			deferred.reject({result: 'Not ok', data: err});
			throw err;
		}
		var response = {result: 'Ok', data: data};
		deferred.resolve(response);
	});
	return deferred.promise;
};


module.exports = Crud;