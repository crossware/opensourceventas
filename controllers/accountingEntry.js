'use strict';

/**
 * @ngdoc function
 * @name IDIapp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of IDIapp
 */
var q = require('q'),
	Crud = require('./crud'),
	soap = require('soap'),
	url = 'http://contabilidad.ngrok.io/Contabilidad/Contabilidad?wsdl',
	MainClass = require('./mainClass');

function AccountingEntry(db) {
	MainClass.apply(this, Array.prototype.slice.call(arguments));
	this.db = db;
	this.crud = new Crud(db, 'INVOICES');
}

AccountingEntry.prototype = new MainClass();


AccountingEntry.prototype.select = function () {
	var deferred = q.defer();


	var args = {
		idAuxiliar: 3,
		idAsiento: null
	};

	console.log(" --- Accounting Entry ---");
	soap.createClient(url, function(err, client) {
      
      client.obtenerTransacciones(args, function(err, result) {
          //console.log("AccountEntry: ", result);
          if (err) {
          	deferred.reject(err);
          } else {
          	console.log("Result: ", result)
          	deferred.resolve(result);
          }
          
      });
  	});

	return deferred.promise;
};

AccountingEntry.prototype.insert = function (invoice) {
	var deferred = q.defer();

	console.log("Server --> AccountingEntry --> Params: ", invoice);

	var args = {
		asiento: {
			auxiliar: 3,
			descripcion: invoice.descripcion,
			entradasContables: [{cuentaContable: 1, tipoMovimiento: "CR", monto: invoice.monto}]
		}
	};

	console.log(" --- Accounting Entry ---");
	soap.createClient(url, function(err, client) {
      
      client.crearAsiento(args, function(err, result) {
          //console.log("AccountEntry: ", result);
          if (err) {
          	deferred.reject(err);
          } else {
          	console.log("Result: ", result)
          	deferred.resolve(result);
          }
          
      });
  	});

	return deferred.promise;
};

AccountingEntry.prototype.update = function () {

};

AccountingEntry.prototype.delete = function () {

};

module.exports = AccountingEntry;