'use strict';

/**
 * @ngdoc function
 * @name IDIapp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of IDIapp
 */
angular.module('IDIapp').controller('InvoiceListCtrl', function ($scope, $state, $http, Invoice, AccountingEntry, invoices, vendors, items) {

	// ----- Variables ----- //
	$scope.$state = $state;
	$scope.invoice = new Invoice();
	$scope.invoices = invoices.data;
	$scope.items = items.data;
	$scope.vendors = vendors.data;
	$scope.item = {};
	$scope.vendor = {};

	var accountingEntry = new AccountingEntry();

	var search = {};
	
	// ----- Objects ----- //
	
	
	// ----- Functions ----- //
	
	// Delete
	$scope.delete = function (item, index) {
		item.delete().then(function (res) {
			$scope.invoices.splice(index, 1);
		}, function (err) {
			console.log('\n --- Not Deleted: ', err);			
		});
	};

	var getInvoices = function () {
		$scope.invoice.select({'query': search}).then(function (res) {
			console.log("--- Custom search: ", res);
			$scope.invoices = res.data;
		});
	};

	$scope.search = function (field, value) {
		search[field] = value;
		getInvoices();
	};

	$scope.removeFilter = function (filter, obj) {
		delete search[filter];
		$scope[obj] = {};
		getInvoices();
	};
	
	// Edit
	$scope.edit = function (id) {
		$state.go('invoice.edit', {id: id});
	};
	
	$scope.back = function () {
		history.go(-1);
	};
	
	$scope.testSoap = function () {
		accountingEntry.select().then(function (response) {
			console.log("Respuesta Soap: ", response);
		}, function (error) {
			console.log("Respuesta Soap (error): ", error);
		});
	};

	// ----- Implemented Code ----- //
	
});