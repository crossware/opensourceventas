'use strict';

/**
 * @ngdoc function
 * @name IDIapp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of IDIapp
 */
angular.module('IDIapp').controller('SessionCtrl', function ($scope, $state, $rootScope) {

	
	window.sessionStorage.isAuthenticated = window.sessionStorage.isAuthenticated || false;
	window.sessionStorage.loadingForFirstTime = window.sessionStorage.loadingForFirstTime || true;
	window.sessionStorage.userData = window.sessionStorage.userData || "{}";
	$rootScope.userData = JSON.parse(window.sessionStorage.userData);

	var loadingForFirstTime = JSON.parse(window.sessionStorage.loadingForFirstTime);
  
	// Se dispara al actualizar de vista
	$rootScope.$on("$stateChangeStart", function (event, toState, toParams, fromState, fromParams) {
		//Pace.start();
		// console.log(' *** isAuthenticated: ', window.sessionStorage.isAuthenticated);
		// console.log(' *** loadingForFirstTime: ', loadingForFirstTime);
		
		if (fromState.name == 'login' && loadingForFirstTime) {
			event.preventDefault();
		} 

		//Si esta logeado y se generaron sus vistas
		if (window.sessionStorage.isAuthenticated == 'true') {
			if (loadingForFirstTime) {
				loadingForFirstTime = false;
				//delete window.sessionStorage.isAuthenticated;
				$state.go(toState.name);
				// Una vez logueado esto pasa a false
			}
		} else {// Si no esta logueado se validan si la ruta esta permitida dentro de alguna de estas
			if (toState.name !== 'login') {
				event.preventDefault();
				$state.go('login');
			}
		}
	});
	
	$rootScope.logOut = function () {
		window.sessionStorage.isAuthenticated = false;
		$state.go('login');
	};
	
});
