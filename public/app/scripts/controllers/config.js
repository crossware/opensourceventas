'use strict';

/**
 * @ngdoc function
 * @name IDIapp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of IDIapp
 */
angular.module('IDIapp').controller('ConfigCtrl', function ($scope, $state, $rootScope, $http, User, Problem, Department, SupportArea) {

	// ----- Variables ----- //
	$scope.$state = $state;
	$scope.roles = [{_id: 1, name: 'Administrador'}, {_id: 2, name: 'Usuario'}];
	
	$scope.problemValid = true;
	$scope.depValid = true;
	$scope.areaValid = true;
	
	
	
	
	// ----- Objects ----- //
	
	// Problem
	$scope.problem = new Problem();
	// Department
	$scope.department = new Department();
	// SupportArea
	$scope.supportArea = new SupportArea();
	
	
	// ----- Functions ----- //
	
	// Save Problem
	$scope.saveProblem = function () {
		if ($scope.problem.name) {
			$scope.problem.save().then(function (res) {
				alert('Agregado satisfactoriamente!');
				$scope.problemValid = true;
				$scope.problem = new Problem();
			});
		} else {
			$scope.problemValid = false;
			//alert('Debe agregar un nombre!');
		}
	};
	
	// Save Department
	$scope.saveDepartment = function () {
		if ($scope.department.name) {
			$scope.department.save().then(function (res) {
				alert('Agregado satisfactoriamente!');
				$scope.depValid = true;
				$scope.department = new Department();
			});
		} else {
			$scope.depValid = false;
			//alert('Debe agregar un nombre!');
		}
	};
	
	// Save Support Area
	$scope.saveSuppArea = function () {
		if ($scope.supportArea.name) {
			$scope.supportArea.save().then(function (res) {
				alert('Agregado satisfactoriamente!');
				$scope.areaValid = true;
				$scope.supportArea = new SupportArea();
			});
		} else {
			$scope.areaValid = false;
			//alert('Debe agregar un nombre!');
		}
	};
	
	
	// Back
	$scope.back = function () {
		history.go(-1);
	};
	
	// ----- Implemented Code ----- //
	
	
});