'use strict';

/**
 * @ngdoc function
 * @name IDIapp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of IDIapp
 */
angular.module('IDIapp').controller('InvoiceCtrl', function ($scope, $state, $rootScope, Invoice, AccountingEntry, $http, invoice, customers, vendors, items) {

	// ----- Variables ----- //
	$scope.$state = $state;
	
	$scope.formValid = true;
	
	$scope.descriptionValid = true;
	$scope.priceValid = true;
	
	$scope.objStatus = 'new';
	
	$scope.items = items.data;
	$scope.customers = customers.data;
	$scope.vendors = vendors.data;

	// ----- Objects ----- //
	var accountingEntry = new AccountingEntry();


	$scope.invoice = new Invoice();
	$scope.invoice.Date = new Date();
	$scope.invoice.Vendor = $rootScope.userData;
	$scope.invoice.VendorId = $rootScope.userData._id;
	
	// ----- Functions ----- //
	
	$scope.validateForm = function () {
		var valid = true;
		if (!$scope.invoice.Vendor) {
			$scope.vendorValid = false;
			valid = false;
		} else {
			$scope.vendorValid = true;
		}
		
		if (!$scope.invoice.Customer) {
			$scope.customerValid = false;
			valid = false;
		} else {
			$scope.customerValid = true;
		}

		if (!$scope.invoice.Item) {
			$scope.itemValid = false;
			valid = false;
		} else {
			$scope.itemValid = true;
		}

		if (!$scope.invoice.Quantity) {
			$scope.quantityValid = false;
			valid = false;
		} else {
			$scope.quantityValid = true;
		}
		return valid;
	};
	
	// Test a http method
	$scope.save = function () {
		//console.log($scope.sRequest);
		if ($scope.validateForm()) {


			accountingEntry.save($scope.invoice.Item.Description, $scope.invoice.Quantity * $scope.invoice.Item.Price).then(function (response) {
				console.log(response);

				$scope.invoice.AccountingEntryId = response.data.asientoResponse.idAsiento;
				
				$scope.invoice.save().then(function (res) {
					alert("Cambios guardados satisfactoriamente!");
				}, function (err) {
					alert('Hubo un error!');
					console.log('\n --- Error: ', err);
				});

			});
			
		}
	};

	$scope.setCustomerId = function () {
		$scope.invoice.CustomerId = $scope.invoice.Customer._id;
	};

	$scope.setItemId = function () {
		$scope.invoice.ItemId = $scope.invoice.Item._id;
	};
	
	// Back
	$scope.back = function () {
		history.go(-1);
	};
	
	// ----- Implemented Code ----- //
	
	if (invoice) {
		$scope.invoice = angular.copy(invoice.data[0]);
	}
	
});