'use strict';

/**
 * @ngdoc function
 * @name IDIapp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of IDIapp
 */
angular.module('IDIapp').controller('ItemCtrl', function ($scope, $state, $rootScope, Item, $http, item) {

	// ----- Variables ----- //
	$scope.$state = $state;
	
	$scope.formValid = true;
	
	$scope.descriptionValid = true;
	$scope.priceValid = true;
	
	$scope.objStatus = 'new';
	
	// ----- Objects ----- //
	$scope.item = new Item();
	
	// ----- Functions ----- //
	
	$scope.validateForm = function () {
		var valid = true;
		if (!$scope.item.Description) {
			$scope.descriptionValid = false;
			valid = false;
		} else {
			$scope.descriptionValid = true;
		}
		if (!$scope.item.Price) {
			console.log($scope.item.Price);
			console.log("Price Invalid");
			$scope.priceValid = false;
			valid = false;
		} else {
			$scope.priceValid = true;
		}
		return valid;
	};
	
	// Test a http method
	$scope.save = function () {
		//console.log($scope.sRequest);
		if ($scope.validateForm()) {
			$scope.item.save().then(function (res) {
				$scope.back();
				//console.log('\n --- Response: ', res);
			}, function (err) {
				alert('Hubo un error!');
				console.log('\n --- Error: ', err);
			});
		}
	};
	
	// Back
	$scope.back = function () {
		history.go(-1);
	};
	
	// ----- Implemented Code ----- //
	
	if (item) {
		$scope.item = angular.copy(item.data[0]);
	}
	
});