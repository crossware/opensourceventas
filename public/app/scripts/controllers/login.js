'use strict';

/**
 * @ngdoc function
 * @name IDIapp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of IDIapp
 */
angular.module('IDIapp').controller('LoginCtrl', function ($scope, $location, $state, $rootScope, User) {

  $scope.showLoginError = false;
  $scope.showLoginEmpty = false;
  $scope.user = new User();
  
  var testUser = {
    _id: "1",
    fullName: 'Abimael Wilmort Fajardo',
    username: 'test',
    password: '1234',
    role: {
      _id: "0",
      name: 'test'
    }
  };

  $scope.submit = function () {

    if (!$scope.user.username || !$scope.user.password) {
      $scope.showLoginEmpty = true;
      $scope.showLoginError = false;
    } else {
      
      if ($scope.user.username == testUser.username && $scope.user.password == testUser.password) {
        window.sessionStorage.isAuthenticated = true;
        window.sessionStorage.userData = JSON.stringify(testUser);
        $rootScope.userData = testUser;
        $state.go('dashboard');
      } else {
        $scope.user.select({query: {username: $scope.user.username, password: $scope.user.password}}).then(function (res) {
          if (res.data.length > 0) {
            window.sessionStorage.isAuthenticated = true;
            window.sessionStorage.userData = JSON.stringify(res.data[0]);
            $rootScope.userData = res.data[0];
            $state.go('dashboard');
          } else {
            $scope.showLoginError = true;
            $scope.showLoginEmpty = false;
          }
          
        }, function (err) {
          
        });
      }
    }
    
    return false;
  };

});
