'use strict';

/**
 * @ngdoc function
 * @name IDIapp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of IDIapp
 */
angular.module('IDIapp').controller('UserListCtrl', function ($scope, $state, $http, users) {

	// ----- Variables ----- //
	$scope.$state = $state;
	$scope.users = users.data;
	
	// ----- Objects ----- //
	
	
	// ----- Functions ----- //
	
	// Delete
	$scope.delete = function (user, index) {
		user.delete().then(function (res) {
			console.log('\n --- Deleted: ', res);
			$scope.users.splice(index, 1);
		}, function (err) {
			console.log('\n --- Not Deleted: ', err);			
		});
	};
	
	// Edit
	$scope.edit = function (id) {
		$state.go('user.edit', {id: id});
	};
	
	$scope.back = function () {
		history.go(-1);
	};
	
	// ----- Implemented Code ----- //
	
	
});