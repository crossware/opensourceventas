'use strict';

/**
 * @ngdoc function
 * @name IDIapp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of IDIapp
 */
angular.module('IDIapp').controller('CustomerCtrl', function ($scope, $state, $rootScope, Customer, $http, customer) {

	// ----- Variables ----- //
	$scope.$state = $state;
	$scope.roles = [{_id: 1, name: 'Administrador'}, {_id: 2, name: 'Usuario'}];
	
	$scope.formValid = true;
	
	$scope.nameValid = true;
	$scope.identificationValid = true;
	$scope.accountValid = true;
	
	$scope.objStatus = 'new';
	
	// ----- Objects ----- //
	$scope.customer = new Customer();
	
	// ----- Functions ----- //
	
	var validateCedula = function (ced) {
		if (ced = "00000000000") {
			alert("Cedula no valida!");
			return false;
		} else {
			var c = ced.replace(/-/g,'');
		    var Cedula = c.substr(0, c.length - 1);
		    var Verificador = c.substr(c.length - 1, 1);
		    var suma = 0;
		    if(ced.length < 13) { return false; }
		    for (i=0;i < Cedula.length;i++) {
		        mod = "";
		         if((i % 2) == 0){mod = 1} else {mod = 2}
		         res = Cedula.substr(i,1) * mod;
		         if (res > 9) {
		              res = res.toString();
		              uno = res.substr(0,1);
		              dos = res.substr(1,1);
		              res = eval(uno) + eval(dos);
		         }
		         suma += eval(res);
		    }
		    el_numero = (10 - (suma % 10)) % 10;

		    if (el_numero == Verificador && Cedula.substr(0,3) != "000") {
		      return true;
		    }
		    else   {
		    	alert("Cedula no valida!");
		    	return false;
		    }
		}
	}


	$scope.validateForm = function () {
		var valid = true;

		if (!$scope.customer.Name) {
			$scope.nameValid = false;
			valid = false;
		} else {
			$scope.nameValid = true;
		}

		if (!$scope.customer.Identification) {
			$scope.identificationValid = validateCedula($scope.customer.Identification.toString());
			valid = validateCedula($scope.customer.Identification.toString());
		} else {
			$scope.identificationValid = true;
		}

		if (!$scope.customer.Account) {
			$scope.accountValid = false;
			valid = false;
		} else {
			$scope.accountValid = true;
		}
		return valid;
	};
	
	// Test a http method
	$scope.save = function () {
		//console.log($scope.sRequest);
		if ($scope.validateForm()) {
			$scope.customer.save().then(function (res) {
				$scope.back();
				//console.log('\n --- Response: ', res);
			}, function (err) {
				alert('Hubo un error!');
				console.log('\n --- Error: ', err);
			});
		}
	};
	
	// Back
	$scope.back = function () {
		history.go(-1);
	};
	
	// ----- Implemented Code ----- //
	
	if (customer) {
		$scope.customer = angular.copy(customer.data[0]);
	}
	
});