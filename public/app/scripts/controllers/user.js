'use strict';

/**
 * @ngdoc function
 * @name IDIapp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of IDIapp
 */
angular.module('IDIapp').controller('UserCtrl', function ($scope, $state, $rootScope, User, $http, user) {

	// ----- Variables ----- //
	$scope.$state = $state;
	$scope.roles = [{_id: 1, name: 'Vendedor'}, {_id: 2, name: 'Administrador'}];
	
	$scope.formValid = true;
	
	$scope.nameValid = true;
	$scope.usernameValid = true;
	$scope.passwordValid = true;
	$scope.roleValid = true;
	
	$scope.objStatus = 'new';
	
	
	
	// ----- Objects ----- //
	$scope.user = new User();
	$scope.user.role = $scope.roles[1];
	
	// ----- Functions ----- //
	
	$scope.validateForm = function () {
		var valid = true;
		if (!$scope.user.fullName) {
			$scope.nameValid = false;
			valid = false;
		} else {
			$scope.nameValid = true;
		}
		if (!$scope.user.username) {
			$scope.usernameValid = false;
			valid = false;
		} else {
			$scope.usernameValid = true;
		}
		if (!$scope.user.password) {
			$scope.passwordValid = false;
			valid = false;
		} else {
			$scope.passwordValid = true;
		}
		if (!$scope.user.role) {
			$scope.roleValid = false;
			valid = false;
		} else {
			$scope.roleValid = true;
		}
		return valid;
	};
	
	// Test a http method
	$scope.save = function () {
		//console.log($scope.sRequest);
		if ($scope.validateForm()) {
			$scope.user.save().then(function (res) {
				$scope.back();
				//console.log('\n --- Response: ', res);
			}, function (err) {
				alert('Hubo un error!');
				console.log('\n --- Error: ', err);
			});
		}
	};
	
	
	
	// Back
	$scope.back = function () {
		history.go(-1);
	};
	
	// ----- Implemented Code ----- //
	
	if (user) {
		$scope.user = angular.copy(user.data[0]);
	}
	
});