'use strict';

/**
 * @ngdoc function
 * @name IDIapp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of IDIapp
 */
angular.module('IDIapp').controller('DashboardCtrl', function ($scope, $state, $rootScope) {
	$scope.$state = $state;
	$scope.user = angular.copy($rootScope.userData);
});
