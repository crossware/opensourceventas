'use strict';

/**
 * @ngdoc function
 * @name IDIapp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of IDIapp
 */
angular.module('IDIapp').controller('ItemListCtrl', function ($scope, $state, $http, items) {

	// ----- Variables ----- //
	$scope.$state = $state;
	$scope.items = items.data;
	
	// ----- Objects ----- //
	
	
	// ----- Functions ----- //
	
	// Delete
	$scope.delete = function (item, index) {
		item.delete().then(function (res) {
			console.log('\n --- Deleted: ', res);
			$scope.users.splice(index, 1);
		}, function (err) {
			console.log('\n --- Not Deleted: ', err);			
		});
	};
	
	// Edit
	$scope.edit = function (id) {
		$state.go('item.edit', {id: id});
	};
	
	$scope.back = function () {
		history.go(-1);
	};
	
	// ----- Implemented Code ----- //
	
	
});