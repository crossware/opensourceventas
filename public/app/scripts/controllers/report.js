'use strict';

/**
 * @ngdoc function
 * @name IDIapp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of IDIapp
 */
angular.module('IDIapp').controller('ReportCtrl', function ($scope, $state, $http, SupportRequest, closedRequestQty) {

	// ----- Variables ----- //

    var defaultChartObjectData = {
        "cols": [
            {id: "t", label: "Topping", type: "string"},
            {id: "s", label: "Solicitudes", type: "number"}
        ],
        "rows": []
    };

	$scope.$state = $state;
    $scope.requestQty = closedRequestQty.data.length;
    $scope.criterios = [
        {label: 'Departamento', option: 'department'},
        {label: 'Problema', option: 'problem'},
        {label: 'Área', option: 'supportArea'}
    ];
    $scope.filterBy = $scope.criterios[0];
    
	//$scope.users = users.data;
	
	// ----- Objects ----- //
	$scope.sRequest = new SupportRequest();
	
	// ----- Functions ----- //
	
    // Filter
    $scope.filter = function () {
        $scope.sRequest.generateReport($scope.filterBy.option).then(function (res) {
            $scope.prepareReport(res.data);
        }, function (err) {          
        });
    };
    
	// Prepare report
	$scope.prepareReport = function (data) {
        $scope.chartObject.options.title = 'Cantidad de Solicitudes por ' + $scope.filterBy.label;
        $scope.chartObject2.options.title = 'Solicitudes por ' + $scope.filterBy.label;
        $scope.chartObject3.options.title = 'Cuántas solicitudes por ' + $scope.filterBy.label + " se han generado:"
        $scope.chartObject.data.rows = [];
        $scope.chartObject2.data.rows = [];
		$scope.chartObject3.data.rows = [];
        for (var x in data) {
            var elem = {c: []};
            elem.c.push({v: data[x]._id});
            elem.c.push({v: data[x].count});
            $scope.chartObject.data.rows.push(elem);
            //$scope.chartObject2.data.rows.push(elem);
        }
	};
	
	$scope.back = function () {
		history.go(-1);
	};
	
	// ----- Implemented Code ----- //
	
    // Initiate the report
    $scope.filter();
    
    $scope.chartObject = {};
    $scope.chartObject2 = {};
	$scope.chartObject3 = {};

    $scope.chartObject.type = "PieChart";
    $scope.chartObject2.type = "BarChart";
    $scope.chartObject3.type = "ColumnChart";
    
    $scope.onions = [
        {v: "Onions"},
        {v: 3},
    ];

    $scope.chartObject.data = defaultChartObjectData;
    $scope.chartObject2.data = defaultChartObjectData;
    $scope.chartObject3.data = defaultChartObjectData;

    $scope.chartObject.options = {
        'title': 'Cantidad de Solicitudes por ' + $scope.filterBy.label
    };

    $scope.chartObject2.options = {
        'title': 'Solicitudes por ' + $scope.filterBy.label
    };
	
    $scope.chartObject3.options = {
        'title': 'Cuántas solicitudes por ' + $scope.filterBy.label + " se han generado:"
    };

});