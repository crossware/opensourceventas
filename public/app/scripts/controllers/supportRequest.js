'use strict';

/**
 * @ngdoc function
 * @name IDIapp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of IDIapp
 */
angular.module('IDIapp').controller('SupportRequestCtrl', function ($scope, $state, $rootScope, SupportRequest, $http, departments, problems, supportAreas, sRequest) {

	// ----- Variables ----- //
	$scope.$state = $state;
	$scope.elems = ['a', 'b', 'c'];
	
	$scope.formValid = true;
	$scope.supportAreaValid = true;
	$scope.problemValid = true;
	$scope.departmentValid = true;
	
	$scope.departments = departments.data;
	$scope.problems = problems.data;
	$scope.supportAreas = supportAreas.data;
	$scope.objStatus = 'new';
	$scope.statuses = ['Abierta', 'Cerrada'];
	
	
	
	// ----- Objects ----- //
	$scope.sRequest = new SupportRequest();
	$scope.sRequest.date = new Date();
	$scope.sRequest.status = $scope.statuses[0];
	
	// ----- Functions ----- //
	
	$scope.validateForm = function () {
		var valid = true;
		if (!$scope.sRequest.problem) {
			$scope.problemValid = false;
			valid = false;
		} else {
			$scope.problemValid = true;
		}
		if (!$scope.sRequest.supportArea) {
			$scope.supportAreaValid = false;
			valid = false;
		} else {
			$scope.supportAreaValid = true;
		}
		if (!$scope.sRequest.department) {
			$scope.departmentValid = false;
			valid = false;
		} else {
			$scope.departmentValid = true;
		}
		return valid;
	};
	
	// Test a http method
	$scope.save = function () {
		//console.log($scope.sRequest);
		if ($scope.validateForm()) {
			$scope.sRequest.save().then(function (res) {
				$scope.back();
				//console.log('\n --- Response: ', res);
			}, function (err) {
				alert('Hubo un error!');
				console.log('\n --- Error: ', err);
			});
		}
	};
	
	
	
	// Back
	$scope.back = function () {
		history.go(-1);
	};
	
	// ----- Implemented Code ----- //
	
	if (sRequest) {
		$scope.objStatus = 'created';
		$scope.sRequest = angular.copy(sRequest.data[0]);
	} else {
		$scope.sRequest.user = angular.copy($rootScope.userData);
	}
	
});