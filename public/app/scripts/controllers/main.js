'use strict';

/**
 * @ngdoc function
 * @name IDIapp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of IDIapp
 */
angular.module('IDIapp').controller('MainCtrl', function ($scope, $state, $http) {

	// ----- Variables ----- //
	$scope.$state = $state;
	
	// ----- Objects ----- //
	
	
	// ----- Functions ----- //
	
	
	
	$scope.back = function () {
		history.go(-1);
	};
	
	// ----- Implemented Code ----- //
	
	
});