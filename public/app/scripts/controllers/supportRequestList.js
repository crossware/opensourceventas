'use strict';

/**
 * @ngdoc function
 * @name IDIapp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of IDIapp
 */
angular.module('IDIapp').controller('SupportRequestListCtrl', function ($scope, $state, $http, supportRequests) {

	// ----- Variables ----- //
	$scope.$state = $state;
	$scope.supportRequests = supportRequests.data;
	
	// ----- Objects ----- //
	
	
	// ----- Functions ----- //
	
	// Delete
	$scope.delete = function (sRequest, index) {
		sRequest.delete().then(function (res) {
			console.log('\n --- Deleted: ', res);
			$scope.supportRequests.splice(index, 1);
		}, function (err) {
			console.log('\n --- Not Deleted: ', err);			
		});
	};
	
	// Edit
	$scope.edit = function (id) {
		$state.go('supportRequest.edit', {id: id});
	};
	
	$scope.back = function () {
		history.go(-1);
	};
	
	// ----- Implemented Code ----- //
	
	
	
});