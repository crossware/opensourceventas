'use strict';

/**
 * @ngdoc function
 * @name IDIapp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of IDIapp
 */
angular.module('IDIapp').controller('CustomerListCtrl', function ($scope, $state, $http, customers) {

	// ----- Variables ----- //
	$scope.$state = $state;
	$scope.customers = customers.data;
	
	// ----- Objects ----- //
	
	
	// ----- Functions ----- //
	
	// Delete
	$scope.delete = function (customer, index) {
		customer.delete().then(function (res) {
			console.log('\n --- Deleted: ', res);
			$scope.users.splice(index, 1);
		}, function (err) {
			console.log('\n --- Not Deleted: ', err);			
		});
	};
	
	// Edit
	$scope.edit = function (id) {
		$state.go('customer.edit', {id: id});
	};
	
	$scope.back = function () {
		history.go(-1);
	};
	
	// ----- Implemented Code ----- //
	
	
});