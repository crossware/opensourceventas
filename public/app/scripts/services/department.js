'use strict';

var app = angular.module('IDIapp');

app.factory('Department', function ($http, Main, $q, $rootScope) {
	
	// Variable que se utiliza para comprobar si un objeto tiene una propiedad
	var hasProp = Object.prototype.hasOwnProperty;
	
	// Nombre de la clase
	var Department;

	function Department(propValues) {
		Department.super.constructor.apply(this, arguments);
		this.baseApiPath = "/Department";
	}

	// Funcion que se utiliza para hacer la herencia desde la clase Main.
	var extend = function (child, parent) {
		var key;
		for (key in parent) {
			if (hasProp.call(parent, key)) {
				child[key] = parent[key];
			}
		}
		function Ctor() {
			this.constructor = child;
		}
		Ctor.prototype = parent.prototype;
		child.prototype = new Ctor();
		child.super = parent.prototype;
		return child;
	};

	// Extender de la clase Main
	extend(Department, Main);

	// Funcion que retorna las propiedades de una cuenta
	Department.properties = function () {
		var r = {};
		return r;
	};

	return Department;

});