'use strict';

var app = angular.module('IDIapp');

app.factory('SupportRequest', function ($http, Main, $q, $rootScope) {

	// Variable que se utiliza para comprobar si un objeto tiene una propiedad
	var hasProp = Object.prototype.hasOwnProperty;
	
	// Nombre de la clase
	var SupportRequest;

	function SupportRequest(propValues) {
		SupportRequest.super.constructor.apply(this, arguments);
		this.baseApiPath = "/SupportRequest";
	}

	// Funcion que se utiliza para hacer la herencia desde la clase Main.
	var extend = function (child, parent) {
		var key;
		for (key in parent) {
			if (hasProp.call(parent, key)) {
				child[key] = parent[key];
			}
		}
		function Ctor() {
			this.constructor = child;
		}
		Ctor.prototype = parent.prototype;
		child.prototype = new Ctor();
		child.super = parent.prototype;
		return child;
	};

	// Extender de la clase Main
	extend(SupportRequest, Main);

	// Funcion que retorna las propiedades de una cuenta
	SupportRequest.properties = function () {
		var r = {};
		return r;
	};


	SupportRequest.prototype.generateReport = function (option) {
		var deferred = $q.defer();
		
		$http.post(this.baseApiPath + '/report', {option: option}).success(function (data, status, headers, config) {
			deferred.resolve(data);
		}).error(function (data, status, headers, config) {
			deferred.reject(data);
		});
		
		return deferred.promise;
	};

	return SupportRequest;

});