'use strict';

var app = angular.module('IDIapp');

app.factory('AccountingEntry', function ($http, Main, $q, $rootScope) {
	
	// Variable que se utiliza para comprobar si un objeto tiene una propiedad
	var hasProp = Object.prototype.hasOwnProperty;
	var urlSoapServer = "http://contabilidad.ngrok.io/Contabilidad/Contabilidad?wsdl";
	
	// Nombre de la clase
	var AccountingEntry;

	function AccountingEntry(propValues) {
		AccountingEntry.super.constructor.apply(this, arguments);
		this.baseApiPath = "/AccountingEntry";
	}

	// Funcion que se utiliza para hacer la herencia desde la clase Main.
	var extend = function (child, parent) {
		var key;
		for (key in parent) {
			if (hasProp.call(parent, key)) {
				child[key] = parent[key];
			}
		}
		function Ctor() {
			this.constructor = child;
		}
		Ctor.prototype = parent.prototype;
		child.prototype = new Ctor();
		child.super = parent.prototype;
		return child;
	};

	// Extender de la clase Main
	extend(AccountingEntry, Main);

	// Funcion que retorna las propiedades de una cuenta
	AccountingEntry.properties = function () {
		var r = {};
		return r;
	};

	AccountingEntry.prototype.select = function () {
		var deferred = $q.defer();

		var params = {};
		$http.post(this.baseApiPath + "/search", {}).then(function(response){
			deferred.resolve(response);
			console.log(response);
		});

		return deferred.promise;
	};

	AccountingEntry.prototype.save = function (descripcion, monto) {
		var deferred = $q.defer();
		var params = {
			descripcion: descripcion,
			monto: monto
		};

		console.log("Service: ", params);
		$http.post(this.baseApiPath, {obj: params}).then(function(response){
			console.log(response);
			deferred.resolve(response);
		}, function (error) {
			deferred.reject(error);
		});

		return deferred.promise;
	};

	// AccountingEntry.prototype.update = function () {

	// };

	// AccountingEntry.prototype.delete = function () {

	// };

	return AccountingEntry;

});