'use strict';

var app = angular.module('IDIapp');

app.factory('Item', function ($http, Main, $q, $rootScope) {
	
	// Variable que se utiliza para comprobar si un objeto tiene una propiedad
	var hasProp = Object.prototype.hasOwnProperty;
	var APIPath = "http://localhost:57867/api/Article";
	
	// Nombre de la clase
	var Item;

	function Item(propValues) {
		Item.super.constructor.apply(this, arguments);
		this.baseApiPath = "http://localhost:57867/api/Article";
	}

	// Funcion que se utiliza para hacer la herencia desde la clase Main.
	var extend = function (child, parent) {
		var key;
		for (key in parent) {
			if (hasProp.call(parent, key)) {
				child[key] = parent[key];
			}
		}
		function Ctor() {
			this.constructor = child;
		}
		Ctor.prototype = parent.prototype;
		child.prototype = new Ctor();
		child.super = parent.prototype;
		return child;
	};

	// Extender de la clase Main
	extend(Item, Main);

	// Funcion que retorna las propiedades de una cuenta
	Item.properties = function () {
		var r = {};
		return r;
	};

	Item.prototype.save = function (id, price, description) {
		var promise,
		_this = this,
      	deferred = $q.defer();

	    if (id != null && id != undefined) {
	      promise = $http.put(_this.baseApiPath, {
	        id: id,
	        price: price,
	        description: description
	      });
	    } else {
	      promise = $http.post(_this.baseApiPath, {
	      	description: description,
	        price: price
	      });
	    }
	    promise.success(function (item, status, headers, config) {
	      return deferred.resolve(_this.successCallback(item, status, headers, config));
	    }).error(function (item, status, headers, config) {
	      return deferred.reject(_this.failureCallback(item, status, headers, config));
	    });
	    return deferred.promise;
	};

	// Select  --------------------------------------------------------------------------------
	Item.prototype.select = function (params) {
		var deferred = $q.defer(),
		  _this = this.constructor;
		
		if (params === null || params == undefined) {
		  params = {};
		}
		console.log(" --- Items --- ");
		$http.get(APIPath, params).success(function (data, status, headers, config) {
		  var response = {},
		    data = data.data;
		    console.log(" --- Items: ", data);
		  //Create a new object of the current class (or an array of them) and return it (or them)
		  if (Array.isArray(data)) {
		    response.data = data.map(function (obj) {
		      return new _this(obj);
		    });
		    //Add "delete" method to results object to quickly delete objects and remove them from the results array
		    response.delete = function (object) {
		      object.delete().then(function () {
		        return response.data.splice(response.data.indexOf(object), 1);
		      });
		    };
		  } else {
		    response = new _this(data);
		  }
		  return deferred.resolve(response);
		}).error(function (data, status, headers, config) {
		  return deferred.reject(data);
		});
		return deferred.promise;
	};

	// Delete ----------------------------------------------------------------------------------
  Main.prototype.delete = function (id) {
    var _this = this,
      url = this.baseApiPath;
    
    var deferred = $q.defer();
    if (!id) {
    	id = this._Id;
    }
    if (id) {
       url = APIPath += "/" + id;
    }
    $http.delete(url, {id: id }).success(function (data, status, headers, config) {
      return deferred.resolve(_this.successCallback(data, status, headers, config));
    }).error(function (data, status, headers, config) {
      return deferred.reject(_this.failureCallback(data, status, headers, config));
    });
    return deferred.promise;
  };

	return Item;

});