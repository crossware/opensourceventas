'use strict';

var app = angular.module('IDIapp');

app.factory('Main', function ($http, $q) {

  // Constructor
  var Main = function (propValues) {
    // Not allow instance of the Main class
    if (this.constructor.name === "Main") {
      throw "The Main class cannot be instantiated and is only meant to be extended by other classes.";
    }
    // Assign properties or instantiate them
    this.assignProperties(propValues);
  };

  // Select  --------------------------------------------------------------------------------
  Main.prototype.select = function (params) {
    var deferred = $q.defer(),
      _this = this.constructor;
		
    if (params === null || params == undefined) {
      params = {};
    }
    
    $http.post(this.baseApiPath + '/search', params).success(function (data, status, headers, config) {
      var response = {},
        data = data.data;
      //Create a new object of the current class (or an array of them) and return it (or them)
      if (Array.isArray(data)) {
        response.data = data.map(function (obj) {
          return new _this(obj);
        });
        //Add "delete" method to results object to quickly delete objects and remove them from the results array
        response.delete = function (object) {
          object.delete().then(function () {
            return response.data.splice(response.data.indexOf(object), 1);
          });
        };
      } else {
        response = new _this(data);
      }
      return deferred.resolve(response);
    }).error(function (data, status, headers, config) {
      return deferred.reject(data);
    });
    return deferred.promise;
  };

  var validate = function () {
    return true;
  };
	
  // Insert -----------------------------------------------------------------------------------
  Main.prototype.save = function (data) {
    var promise,
      _this = this,
      deferred = $q.defer();

    if (data == null || data == undefined) {
      delete this.errors;
      data = this.getDataForApi();
    }
    delete data.baseApiPath;
    if (this._id != null && this._id != undefined) {
      promise = $http.put(_this.baseApiPath, {
        id: this._id,
        obj: data
      });
    } else {
      promise = $http.post(_this.baseApiPath, {
        obj: data
      });
    }
    promise.success(function (data, status, headers, config) {
      return deferred.resolve(_this.successCallback(data, status, headers, config));
    }).error(function (data, status, headers, config) {
      return deferred.reject(_this.failureCallback(data, status, headers, config));
    });
    return deferred.promise;
  };

  // Update -------------------------------------------------------------------------------------
  Main.prototype.update = function (data) {
    var promise,
      _this = this,
      deferred = $q.defer();;
    if (data == null || data == undefined) {
      data = this.getDataForApi();
    }
    if (validate()) {
      if (_this._id != null) {
        promise = $http.put("" + _this.baseApiPath, {
          id: _this._id,
          obj: data
          //,query: params
        });
      }
      promise.success(function (data, status, headers, config) {
        return deferred.resolve(_this.successCallback(data, status, headers, config));
      }).error(function (data, status, headers, config) {
        return deferred.reject(_this.failureCallback(data, status, headers, config));
      });
    } else {
      deferred.reject(new Error('Invalid Object'));
    }
    return deferred.promise;
  };

  // Delete ----------------------------------------------------------------------------------
  Main.prototype.delete = function (params) {
    var _this = this,
      url = this.baseApiPath;
    if (params == null) {
      params = {};
    }
    var deferred = $q.defer();
    if (this._id) { 
       url = this.baseApiPath += "/" + this._id;
    }
    $http.delete(url, {query: params }).success(function (data, status, headers, config) {
      return deferred.resolve(_this.successCallback(data, status, headers, config));
    }).error(function (data, status, headers, config) {
      return deferred.reject(_this.failureCallback(data, status, headers, config));
    });
    return deferred.promise;
  };

	


  Main.prototype.assignProperties = function (data) {
    // Variables
    var _this = this;
    data = convertDate(data);
    // Look for the property value
    var getPropertyValue = function (_defaultValue, _value) {
      // Check if this property should be an instance of another class
      if (_defaultValue != null && typeof _defaultValue === "function") {
        // Check if it is just an insance or an array of instances
        if (Array.isArray(_value)) {
          return _value.map(function (obj) {
            return new _defaultValue(obj);
          });
        } else {
          return new defaultValue(_value);
        }
        // If it is not an instance just assign everything
      } else if (typeof _value == "object") {
        return convertDate(_value);
      } else {
        return _value;
      }
    };

    // Business Logic
    if (data == null) {
      data = {};
    }

    var properties = this.constructor.properties();

    // Look for each property in the class
    for (var key in data) {

      // Get default value / constructor
      var defaultValue = properties[key];

      _this[key] = getPropertyValue(defaultValue, data[key]);
    };

    // return the incoming data in case some other function wants to play with it next        
    return data;
  };

  Main.prototype.getDataForApi = function (object) {
    if (object == null) {
      object = this;
    }
    delete object.errors;
    return JSON.parse(JSON.stringify(object));
  };


  /*
    Callbacks for $http response promise
  */

  Main.prototype.successCallback = function (data, status, headers, config) {
    return this.assignProperties(data.data || data);
  };

  Main.prototype.failureCallback = function (data, status, headers, config) {
    return this.assignErrors(data.error || data);
  };

  Main.prototype.assignErrors = function (errorData) {
    return this.errors = errorData;
  };

  function convertDate(object) {
    var key;
    for (key in object) {

      if (/date/.test(key.toLowerCase())) {
        object[key] = new Date(object[key]);

        if (isNaN(object[key])) {
          object[key] = undefined;
        }

      }
    }
    return object;
  }

  return Main;

});