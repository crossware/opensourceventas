'use strict';

var app = angular.module('IDIapp');

app.factory('SupportArea', function ($http, Main, $q, $rootScope) {
	
	// Variable que se utiliza para comprobar si un objeto tiene una propiedad
	var hasProp = Object.prototype.hasOwnProperty;
	
	// Nombre de la clase
	var SupportArea;

	function SupportArea(propValues) {
		SupportArea.super.constructor.apply(this, arguments);
		this.baseApiPath = "/SupportArea";
	}

	// Funcion que se utiliza para hacer la herencia desde la clase Main.
	var extend = function (child, parent) {
		var key;
		for (key in parent) {
			if (hasProp.call(parent, key)) {
				child[key] = parent[key];
			}
		}
		function Ctor() {
			this.constructor = child;
		}
		Ctor.prototype = parent.prototype;
		child.prototype = new Ctor();
		child.super = parent.prototype;
		return child;
	};

	// Extender de la clase Main
	extend(SupportArea, Main);

	// Funcion que retorna las propiedades de una cuenta
	SupportArea.properties = function () {
		var r = {};
		return r;
	};

	return SupportArea;

});