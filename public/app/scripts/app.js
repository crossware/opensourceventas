'use strict';

/**
 * @ngdoc overview
 * @name IDIapp
 * @description
 * # IDIapp
 *
 * Main module of the application.
 */
angular.module('IDIapp', [
    'ui.router',
    'ngAnimate',
	'googlechart'
]).config(function ($stateProvider, $urlRouterProvider) {

	$urlRouterProvider.when('/dashboard', '/dashboard/overview');
	$urlRouterProvider.otherwise('/login');

	$stateProvider.state('base', {
			abstract: true,
			url: '',
			templateUrl: 'views/base.html'

		}).state('login', {
			url: '/login',
			parent: 'base',
			templateUrl: 'views/login.html',
			controller: 'LoginCtrl'
		})
		.state('dashboard', {
			url: '/dashboard',
			parent: 'base',
			templateUrl: 'views/dashboard.html',
			controller: 'DashboardCtrl'

		}).state('main', {
			url: '/main',
			parent: 'dashboard',
			templateUrl: 'views/main.html',
			controller: 'MainCtrl'
		}).state('overview', {
			url: '/overview',
			parent: 'dashboard',
			templateUrl: 'views/dashboard/overview.html'

		}).state('reports', {
			url: '/reports',
			parent: 'dashboard',
			templateUrl: 'views/dashboard/reports.html',
			controller: 'ReportCtrl',
			resolve: {
				closedRequestQty: function (SupportRequest) {
					return new SupportRequest().select({query: {status: 'Cerrada'}});
				}
			}

		}).state('supportRequest', {
			url: '/supportRequest',
			parent: 'dashboard',
			templateUrl: 'views/supportRequest.html',
			controller: 'SupportRequestCtrl',
			resolve: {
				sRequest: function () {
					return null;
				},
				departments: function (Department) {
					return new Department().select();
				},
				problems: function (Problem) {
					return new Problem().select();
				},
				supportAreas: function (SupportArea) {
					return new SupportArea().select();
				}
			}
		}).state('supportRequest.list', {
			url: '/supportRequest/list',
			parent: 'dashboard',
			templateUrl: 'views/supportRequestList.html',
			controller: 'SupportRequestListCtrl',
			resolve: {
				supportRequests: function (SupportRequest) {
					return new SupportRequest().select();
				}
			}
		}).state('supportRequest.edit', {
			url: '/supportRequest/edit/:id',
			parent: 'dashboard',
			templateUrl: 'views/supportRequest.html',
			controller: 'SupportRequestCtrl',
			resolve: {
				departments: function (Department) {
					return new Department().select();
				},
				problems: function (Problem) {
					return new Problem().select();
				},
				supportAreas: function (SupportArea) {
					return new SupportArea().select();
				},
				sRequest: function ($stateParams, SupportRequest) {
					var query = {'_id': $stateParams.id};
					return new SupportRequest().select({query: query});
				}
			}
		}).state('user', {
			url: '/user',
			parent: 'dashboard',
			templateUrl: 'views/user.html',
			controller: 'UserCtrl',
			resolve: {
				user: function () {
					return null;
				}
			}
		}).state('user.edit', {
			url: '/user/edit/:id',
			parent: 'dashboard',
			templateUrl: 'views/user.html',
			controller: 'UserCtrl',
			resolve: {
				user: function ($stateParams, User) {
					var query = {'_id': $stateParams.id};
					return new User().select({query: query});
				}
			}
		}).state('user.list', {
			url: '/user/list',
			parent: 'dashboard',
			templateUrl: 'views/userList.html',
			controller: 'UserListCtrl',
			resolve: {
				users: function (User) {
					return new User().select();
				}
			}
		}).state('item', {
			url: '/item',
			parent: 'dashboard',
			templateUrl: 'views/item.html',
			controller: 'ItemCtrl',
			resolve: {
				item: function () {
					return null;
				}
			}
		}).state('item.list', {
			url: '/item/list',
			parent: 'dashboard',
			templateUrl: 'views/itemList.html',
			controller: 'ItemListCtrl',
			resolve: {
				items: function (Item) {
					return new Item().select();
				}
			}
		}).state('item.edit', {
			url: '/item/edit/:id',
			parent: 'dashboard',
			templateUrl: 'views/item.html',
			controller: 'ItemCtrl',
			resolve: {
				item: function ($stateParams, Item) {
					var query = {'_id': $stateParams.id};
					return new Item().select({query: query});
				}
			}
		}).state('customer', {
			url: '/customer',
			parent: 'dashboard',
			templateUrl: 'views/customer.html',
			controller: 'CustomerCtrl',
			resolve: {
				customer: function () {
					return null;
				}
			}
		}).state('customer.list', {
			url: '/customer/list',
			parent: 'dashboard',
			templateUrl: 'views/customerList.html',
			controller: 'CustomerListCtrl',
			resolve: {
				customers: function (Customer) {
					return new Customer().select();
				}
			}
		}).state('customer.edit', {
			url: '/customer/edit/:id',
			parent: 'dashboard',
			templateUrl: 'views/customer.html',
			controller: 'CustomerCtrl',
			resolve: {
				customer: function ($stateParams, Customer) {
					var query = {'_id': $stateParams.id};
					return new Customer().select({query: query});
				}
			}
		}).state('invoice', {
			url: '/invoice',
			parent: 'dashboard',
			templateUrl: 'views/invoice.html',
			controller: 'InvoiceCtrl',
			resolve: {
				invoice: function () {
					return null;
				},
				customers: function (Customer) {
					return new Customer().select();
				},
				vendors: function (User) {
					return new User().select();
				},
				items: function (Item) {
					return new Item().select();
				}
			}
		}).state('invoice.list', {
			url: '/invoice/list',
			parent: 'dashboard',
			templateUrl: 'views/invoiceList.html',
			controller: 'InvoiceListCtrl',
			resolve: {
				invoices: function (Invoice) {
					return new Invoice().select();
				},
				vendors: function (User) {
					return new User().select();
				},
				items: function (Item) {
					return new Item().select();
				}
			}
		}).state('invoice.edit', {
			url: '/invoice/edit/:id',
			parent: 'dashboard',
			templateUrl: 'views/invoice.html',
			controller: 'InvoiceCtrl',
			resolve: {
				invoice: function ($stateParams, Invoice) {
					var query = {'_id': $stateParams.id};
					return new Invoice().select({query: query});
				},
				customers: function (Customer) {
					return new Customer().select();
				},
				vendors: function (User) {
					return new User().select();
				},
				items: function (Item) {
					return new Item().select();
				}
			}
		}).state('config', {
			url: '/config',
			parent: 'dashboard',
			templateUrl: 'views/config.html',
			controller: 'ConfigCtrl',
			resolve: {
			}
		});

});