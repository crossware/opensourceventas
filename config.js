var config = {
	development: {
		DB_URL: '127.0.0.1:27017/SUPPORTDB',
		APP_PORT: process.env.PORT || 9000
	},

	production: {
		DB_URL: '127.0.0.1:27017/SUPPORTDB',
		APP_PORT: process.env.PORT || 9000
	}
};

function init(app) {
	var mode = app.get('env');
	return config[mode];
}

exports.init = init;