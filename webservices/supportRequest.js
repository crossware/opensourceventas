'use strict';

/**
 */
var q = require('q'),
	util = require('../controllers/util'),
	SupportRequest = require('../controllers/supportRequest');


module.exports = function (prefix, app, db) {
	
	// Generate Report
	app.post(prefix + '/report', function (req, res) {
		var sRequest = new SupportRequest(db);
		sRequest.generateReport(req.body.option).then(util.success(res), util.error(res));
	});
	
	require('./crudServices')(prefix, app, db, SupportRequest);
};