'use strict';

/**
 */
var q = require('q'),
	util = require('../controllers/util'),
	Invoice = require('../controllers/invoice');


module.exports = function (prefix, app, db) {
	
	require('./crudServices')(prefix, app, db, Invoice);
};