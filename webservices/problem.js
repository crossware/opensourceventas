'use strict';

/**
 */
var q = require('q'),
	util = require('../controllers/util'),
	Problem = require('../controllers/problem');


module.exports = function (prefix, app, db) {
	
	require('./crudServices')(prefix, app, db, Problem);
};