'use strict';

/**
 */
var q = require('q'),
	util = require('../controllers/util'),
	Customer = require('../controllers/customer');


module.exports = function (prefix, app, db) {
	
	require('./crudServices')(prefix, app, db, Customer);
};