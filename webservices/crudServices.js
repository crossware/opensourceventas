'use strict';

/**
 */
var q = require('q'),
	util = require('../controllers/util');


module.exports = function (prefix, app, db, ParentClass) {
	
	// Insert
	app.post(prefix, function (req, res) {
		var obj = new ParentClass(db);
		obj.insert(req.body.obj).then(util.success(res), util.error(res));
	});
	
	// Update
	app.put(prefix, function (req, res) {
		var obj = new ParentClass(db);
		obj.update(req.body.id, req.body.obj).then(util.success(res), util.error(res));
	});
	
	// Delete by Id
	app.delete(prefix + '/:id', function (req, res) {
		var obj = new ParentClass(db);
		obj.delete(req.params.id).then(util.success(res), util.error(res));
	});
	
	// Search
	app.post(prefix + '/search', function (req, res) {
		var obj = new ParentClass(db);
		console.log(prefix, " ",req.body);
		obj.select(req.body.query).then(util.success(res), util.error(res));
	});
	
};