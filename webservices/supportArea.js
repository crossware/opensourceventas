'use strict';

/**
 */
var q = require('q'),
	util = require('../controllers/util'),
	SupportArea = require('../controllers/supportArea');


module.exports = function (prefix, app, db) {
	
	require('./crudServices')(prefix, app, db, SupportArea);
};