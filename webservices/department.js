'use strict';

/**
 */
var q = require('q'),
	util = require('../controllers/util'),
	Department = require('../controllers/department');


module.exports = function (prefix, app, db) {
	
	require('./crudServices')(prefix, app, db, Department);
};