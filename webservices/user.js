'use strict';

/**
 */
var q = require('q'),
	util = require('../controllers/util'),
	User = require('../controllers/user');


module.exports = function (prefix, app, db) {
	
	require('./crudServices')(prefix, app, db, User);
};