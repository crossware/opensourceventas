'use strict';

/**
 */
var q = require('q'),
	util = require('../controllers/util'),
	AccountingEntry = require('../controllers/accountingEntry');


module.exports = function (prefix, app, db) {
	
	require('./crudServices')(prefix, app, db, AccountingEntry);
};