'use strict';

/**
 */
var q = require('q'),
	util = require('../controllers/util'),
	Item = require('../controllers/item');


module.exports = function (prefix, app, db) {
	
	require('./crudServices')(prefix, app, db, Item);
};