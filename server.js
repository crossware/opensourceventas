var express = require('express'),
	app = express(),
	http = require('http'),
	validator = require('express-validator'),
	path = require('path'),
	config = require('./config').init(app),
	db = require('monk')(config.DB_URL);


var d = require('domain').create();

//
d.on('error', function (er) {
	console.log('----ERROR----');
	console.error(er.message);
	console.error(er.stack);
	var error = {
		fecha: new Date(),
		mensaje: er.message,
		stack: er.stack
	};
	db.get('LOGERROR').insert(error, function (err) {
		if (err) {
			//Poner error en archivo log-dia
			var fs = require('fs');
			var urlfs = __dirname + "/log/log-" + new Date().getFullYear() + "-" + (parseInt(new Date().getMonth()) + 1) + "-" + new Date().getDate() + ".txt";
			var txtError = "Fecha: " + error.fecha + ", Mensaje: " + error.mensaje + ", Stack: " + error.stack + "\n";
			fs.appendFile(urlfs, txtError, function (err) {
				if (err) {
					console.log(err);
				} else {
					console.log("The log was saved in " + urlfs);
				}
			});
		}
	});
});

d.run(function () {

	var secret = "asd243131";

	app.configure(function () {
		app.use(express.logger());
		app.use(express.methodOverride());
		//app.use('/api', auth({secret: secret }));
		app.use(express.json());
		app.use(express.urlencoded());
		app.use(validator());
		app.use(app.router);
		app.use(express.responseTime());
		app.use(express.compress());
		app.use(express.favicon());
		app.use('/', express.static(path.join(__dirname, 'public/app')));
		app.use('/images', express.static(path.join(__dirname, 'images')));
	});

	app.configure('development', function () {
		app.use(express.errorHandler({
			dumpExceptions: true,
			showStack: true
		}));
	});

	app.configure('production', function () {
		app.use(express.errorHandler());
	});

	// WebServices
	require('./webservices/supportRequest')('/SupportRequest', app, db);
	require('./webservices/department')('/Department', app, db);
	require('./webservices/problem')('/Problem', app, db);
	require('./webservices/supportArea')('/SupportArea', app, db);
	require('./webservices/user')('/User', app, db);
	require('./webservices/item')('/Item', app, db);
	require('./webservices/customer')('/Customer', app, db);
	require('./webservices/invoice')('/Invoice', app, db);
	require('./webservices/accountingEntry')('/AccountingEntry', app, db);

	//-- ROUTES
	app.get('/api/routes', function (req, res) {
		res.send(app.routes);
	});
	//app.get('/api/profile', users.profile(db));

	//API UPLOAD FILES
	app.post('/upload/image', function (req, res) {
		var http = require('http');
		var fs = require('fs');
		var type = "." + req.body.url.split('.').pop();
		var path = "images/" + req.body.obj + type; //Le pasas un archivo en comun llamado obj

		if (fs.existsSync(path)) {
			res.json({
				url: path
			});
		} else {
			var request = http.get(req.body.url, function (resUrl) {
				if (resUrl.statusCode == 200) {
					var file = fs.createWriteStream(path);
					resUrl.pipe(file);
					res.json({
						url: path
					});
				} else {
					res.json({
						url: 'images/noLogo.jpg'
					});
				}
			}).on('error', function (e) {
				res.json({
					url: 'images/noLogo.jpg'
				});
			});
		}
	});

	http.createServer(app).listen(config.APP_PORT, function () {
		//ws.autoLogin();
		console.log("\n[*] Server Listening on port %d", config.APP_PORT);
	});


});